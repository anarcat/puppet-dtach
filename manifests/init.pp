# base class for detached programs
#
# This will be common to all programs managed by dtach
class dtach(
  Enum['present','absent'] $ensure = 'present',
) {
  package { 'dtach':
    ensure => $ensure,
  }
  file { '/usr/local/bin/mosh-ssh-wrapper':
    ensure => $ensure,
    source => 'puppet:///modules/dtach/mosh-ssh-wrapper',
    mode   => '0555',
  }
}
