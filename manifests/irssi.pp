# run irssi detached under less privileges
#
# @param user if set, will also enable a session for that user
class dtach::irssi(
  Enum['present','absent'] $ensure = 'present',
  Optional[String] $user = undef,
) {
  class { 'dtach':
    ensure => $ensure,
  }
  package { 'irssi':
    ensure => $ensure,
  }
  if $ensure == 'present' {
    systemd::unit_file { 'irssi@.service':
      source  => 'puppet:///modules/dtach/irssi@.service',
      enable  => false,
      active  => false,
      require => Package['irssi'],
    }
  } else {
    systemd::unit_file { 'irssi@.service':
      ensure => $ensure,
      source => 'puppet:///modules/dtach/irssi@.service',
    }
  }
  # TODO: could be a list if we want to have multiple users in
  # hiera...
  if $user {
    dtach::irssi::session { $user :
      ensure => $ensure,
    }
  }
}
