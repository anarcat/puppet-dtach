# enable a single user session
define dtach::irssi::session(
  Enum['present','absent'] $ensure = 'present',
) {
  if $ensure == 'present' {
    systemd::unit_file { "irssi@${name}.service":
      target => '/etc/systemd/system/irssi@.service',
      path   => '/etc/systemd/system/multi-user.target.wants/',
      enable => true,
      active => true,
    }
  } else {
    systemd::unit_file { "irssi@${name}.service":
      ensure => absent,
      path   => '/etc/systemd/system/multi-user.target.wants/',
    }
  }
  user { $name:
    ensure     => $ensure,
    system     => true,
    managehome => true,
    shell      => '/bin/sh',
    comment    => "${name} IRC bouncer",
  }
}
